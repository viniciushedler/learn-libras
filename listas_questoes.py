from questao import Questao
# q_ = Questao(, "", "q_.jpg", ['','','',''], '')
q0_0 = Questao(0, "1- Qual letra o seguinte sinal representa?", "q0_0.jpg", ["U", "J", "K", "V"], "J")
q0_1 = Questao(1, "2- Qual letra o seguinte sinal representa?","q0_1.jpg.", ['Y','F','R','T'], "Y")
q0_2 = Questao(2, "3- Qual letra o seguinte sinal representa?","q0_2.jpg", ['O','E','U','A'], "A")
q0_3 = Questao(3, "4- Qual letra o seguinte sinal representa?","q0_3.jpg", ['J','I','G','L'], "G")
q0_4 = Questao(4, "5- Qual letra o seguinte sinal representa?","q0_4.jpg",['R','U','W','H'], 'U')
q0_5 = Questao(5, "6- Qual letra o seguinte sinal representa?","q0_5.jpg",['Y','X','V','N'], 'V')
q0_6 = Questao(6, "7- Qual letra o seguinte sinal representa?","q0_6.jpg",['R','P','Q','S'], 'R')
q0_7 = Questao(7, "8- Qual letra o seguinte sinal representa?","q0_7.jpg",['K','T','J','F'], 'F')
q0_8 = Questao(8, "9- Qual letra o seguinte sinal representa?","q0_8.jpg",['Q','K','F','T'], 'Q')
q0_9 = Questao(9, "10- Qual letra o seguinte sinal representa?","q0_9.jpg",['N','E','B','S'], 'S')
q0_10 = Questao(10, "11- Qual letra o seguinte sinal representa?","q0_10.jpg",['L','P','O','X'], 'P')
q0_11 = Questao(11, "12- Qual letra o seguinte sinal representa?","q0_11.jpg",['K','X','U','M'], 'X')
q0_12 = Questao(12, "13- Qual letra o seguinte sinal representa?","q0_12.jpg",['M','T','N','D'], 'T')
q0_13 = Questao(13, "14- Qual letra o seguinte sinal representa?","q0_13.jpg",['K','G','Z','B'], 'B')
q0_14 = Questao(14, "15- Qual letra o seguinte sinal representa?","q0_14.jpg",['E','W','A','B'], 'W')
q0_15 = Questao(15, "16- Qual letra o seguinte sinal representa?","q0_15.jpg",['R','O','B','Z'], 'O')
q0_16 = Questao(16, "17- Qual letra o seguinte sinal representa?","q0_16.jpg",['I','V','N','T'], 'N')
q0_17 = Questao(17, "18- Qual letra o seguinte sinal representa?","q0_17.jpg",['V','I','H','C'], 'I')
q0_18 = Questao(18, "19- Qual letra o seguinte sinal representa?","q0_18.jpg",['K','E','S','X'], 'E')
q0_19 = Questao(19, "20- Qual letra o seguinte sinal representa?","q0_19.jpg",['P','L','N','Z'], 'Z')
q0_20 = Questao(20, "21- Qual letra o seguinte sinal representa?","q0_20.jpg",['C','S','G','H'], 'H')
q0_21 = Questao(21, "22- Qual letra o seguinte sinal representa?","q0_21.jpg",['G','Y','C','A'], 'C')
q0_22 = Questao(22, "23- Qual letra o seguinte sinal representa?","q0_22.jpg",['B','C','K','M'], 'M')
q0_23 = Questao(23, "24- Qual letra o seguinte sinal representa?","q0_23.jpg",['K','V','U','J'], 'K')
q0_24 = Questao(24, "25- Qual letra o seguinte sinal representa?","q0_24.jpg",['E','I','S','L'], 'L')
q0_25 = Questao(25, "26- Qual letra o seguinte sinal representa?","q0_25.jpg",['D','R','H','B'], 'D')
questoes_alfabeto = [q0_0,q0_1,q0_2,q0_3,q0_4,q0_5,q0_6,q0_7,q0_8,q0_9,q0_10,q0_11,q0_12,q0_13,q0_14,q0_15,q0_16,q0_17,q0_18,q0_19,q0_20,q0_21,q0_22,q0_23,q0_24,q0_25]
# questoes_alfabeto=[q0_1,q0_2,q0_3]




q1_0 = Questao(0, "1- Qual número o seguinte sinal representa?", "q1_0.jpg", ['4','10','3','5'], '5')
q1_1 = Questao(1, "2- Qual número o seguinte sinal representa?", "q1_1.jpg", ['10','6','8','7'], '8')
q1_2 = Questao(2, "3- Qual número o seguinte sinal representa?", "q1_2.jpg", ['7','9','10','8'], '9')
q1_3 = Questao(3, "4- Qual número o seguinte sinal representa?", "q1_3.jpg", ['3','7','4','10'], '4')
q1_4 = Questao(4, "5- Qual número o seguinte sinal representa?", "q1_4.jpg", ['2','10','1','6'], '2')
q1_5 = Questao(5, "6- Qual número o seguinte sinal representa?", "q1_5.jpg", ['9','3','1','5'], '1')
q1_6 = Questao(6, "7- Qual número o seguinte sinal representa?", "q1_6.jpg", ['7','4','6','10'], '7')
q1_7 = Questao(7, "8- Qual número o seguinte sinal representa?", "q1_7.jpg", ['5','9','3','2'], '3')
q1_8 = Questao(8, "9- Qual número o seguinte sinal representa?", "q1_8.jpg", ['5','10','6','1'], '6')
questoes_numeros = [q1_0,q1_1,q1_2,q1_3,q1_4,q1_5,q1_6,q1_7,q1_8]




listas = [questoes_alfabeto, questoes_numeros]